# 3/22/2021

### df66ad99b43b7cfbae3bb608d841517e58fd181f

---

## Flaws

* На інтерактивній області не малюються проміжні лінії, візуально не видно контурів поки фігура не намальована повністю
* Також варто було б додати оновлення інтерактивної області після закриття вікна обрахунку

### File [package.json](package.json)

* `npx` використовується для запуску команд, якщо відповідний пакет не встановлено локально. В проекті є `package.json` значить треба встановити локальні пакети. Команду `compile` можна переписати `"compile": "tsc"`. Причина: `npx` завантажує останню версію `typescript`, для проекту може бути потрібна зовсім інша версія, тому треба використовувати як є в `package.json`

### File [styles.css](styles.css)

* ~1-2 Обнуляти відступи треба тільки там де треба, глобальні зміни можуть бути негативними, наприклад, при використанні сторонніх пакетів з своїми стилями
* ~41 Керування видимістю має бути дещо інше. Потрібно дописати класс `.hidden`, вказати йому `display: none;` і добавляти його для елемента, який треба приховати

### File [index.ts](index.ts)

* ~8 Масив з чисел складно аналізувати. Можна створити окремий інтерфейс `Point` з властивостями `x`, `y`
* ~34 Непотрібно зайвий раз присвоювати значення і ніде його не використовувати. Можна зразу писати 
  ```typescript
  let result = Math.sqrt();
  ```
* ~92 Спочатку треба розмістити контент, тоді показувати вікно. Не навпаки
* ~91 Краще додавати/забирати клас `.hidden`. Чому: нема гарантії, що елемент бути блоковий, а може це буде `flex`
* ~57 Мініфіковані назви змінних важко аналізувати. Для розробки варто писати повноцінні назви, щоб одразу можна було розуміти призначення 
* ~102 функція handleConfirm не варто переривати виконання функції пустим return, варто щось з нього повертати, або придумати іншу логику, яка буде більш читабельною.

### File [index.html](index.html)

* 4 пробіли забагато для відступів, займає багато простору і не має позитивного ефекту

# 23/3/2021

### 4bd8d1cc1e9c3b433d3ea8a576a2184438ca45ec

---

## Flaws 

* Структура не відповідає звичній для розробки. Будь ласка, каталог `src` з модульними файлами `.ts`, які окремо розділяють методи обробки запитів, інтерфейси і типи, функції-помічники і файл вхідних скриптів. Результат компіляції файлів має знаходитись в директорії `/public`

### File [index.ts](index.ts)

* Немає типів у змінних і констант

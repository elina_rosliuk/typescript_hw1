const panel = document.querySelector('.panel');
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
export const setUpCanvasSize = () => {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight - panel.offsetHeight;
};
setUpCanvasSize();
export const drawPoint = (pointX, pointY) => {
    ctx.beginPath();
    ctx.arc(pointX, pointY, 2, 0, 2 * Math.PI, true);
    ctx.fillStyle = '#000';
    ctx.fill();
    ctx.closePath();
};
export const drawFigure = (coords) => {
    ctx.fillStyle = '#4a4e69';
    ctx.beginPath();
    ctx.moveTo(coords[0].x, coords[0].y);
    for (let i = 1; i < coords.length; i++) {
        ctx.lineTo(coords[i].x, coords[i].y);
    }
    ctx.closePath();
    ctx.fill();
};
export const clearInterface = (coords) => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    coords.length = 0;
};

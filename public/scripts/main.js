import { polygonArea, calculatePerimeter } from './helpers.js';
import { drawPoint, drawFigure, clearInterface, setUpCanvasSize, } from './canvas.js';
const overlay = document.querySelector('.overlay');
const modal = document.querySelector('.modal');
const modalContent = document.querySelector('.modal-content');
export const showModal = (content, error = false) => {
    modalContent.innerHTML = content;
    overlay.classList.remove('hidden');
    modal.classList.remove('hidden');
    if (error) {
        modal.classList.add('error');
    }
};
export const handleCloseModal = () => {
    overlay.classList.add('hidden');
    modal.classList.add('hidden');
    if (!modal.classList.contains('error')) {
        clearInterface(coords);
    }
    modal.classList.remove('error');
};
const coords = [];
export const handleClick = (event) => {
    if (coords.length <= 100) {
        const pointX = event.clientX, pointY = event.clientY;
        drawPoint(pointX, pointY);
        coords.push({ x: pointX, y: pointY });
    }
    else {
        showModal('Maximum 100 points are allowed!', true);
    }
};
export const handleConfirm = () => {
    if (coords.length > 2) {
        drawFigure(coords);
        const perimeter = calculatePerimeter(coords);
        const area = polygonArea(coords);
        coords.length = 0;
        showModal(`Perimeter: ${perimeter} <br /> Area: ${area}`);
    }
    else {
        showModal('Minimum 3 points are needed!', true);
    }
};
export const handleClear = () => {
    clearInterface(coords);
};
export const handleResize = () => {
    setUpCanvasSize();
};

export const roundNumber = (number) => {
    return Number((Math.round(number * 100) / 100).toFixed(2));
};
export const polygonArea = (coords) => {
    const numPoints = coords.length;
    let area = 0;
    let lastElement = numPoints - 1;
    for (let i = 0; i < numPoints; i++) {
        area +=
            (coords[lastElement].x + coords[i].x) *
                (coords[lastElement].y - coords[i].y);
        lastElement = i;
    }
    if (area < 0) {
        area *= -1;
    }
    return roundNumber(area / 2);
};
export const calculatePerimeter = (coords) => {
    const initialX = coords[0].x - coords[coords.length - 1].x;
    const initialY = coords[0].y - coords[coords.length - 1].y;
    let result = Math.sqrt(Math.pow(initialX, 2) + Math.pow(initialY, 2));
    for (let i = 1; i < coords.length; i++) {
        const vectorX = coords[i].x - coords[i - 1].x;
        const vectorY = coords[i].y - coords[i - 1].y;
        const length = Math.sqrt(Math.pow(vectorX, 2) + Math.pow(vectorY, 2));
        result += length;
    }
    return roundNumber(result);
};

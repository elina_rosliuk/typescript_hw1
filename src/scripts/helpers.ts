import { CoordsType } from './types.js';

export const roundNumber = (number: number): number => {
  return Number((Math.round(number * 100) / 100).toFixed(2));
};

export const polygonArea = (coords: CoordsType): number => {
  const numPoints: number = coords.length;
  let area: number = 0;
  let lastElement: number = numPoints - 1;

  for (let i = 0; i < numPoints; i++) {
    area +=
      (coords[lastElement].x + coords[i].x) *
      (coords[lastElement].y - coords[i].y);
    lastElement = i; //j is previous vertex to i
  }

  if (area < 0) {
    area *= -1;
  }

  return roundNumber(area / 2);
};

export const calculatePerimeter = (coords: CoordsType): number => {
  const initialX: number = coords[0].x - coords[coords.length - 1].x;
  const initialY: number = coords[0].y - coords[coords.length - 1].y;

  let result: number = Math.sqrt(Math.pow(initialX, 2) + Math.pow(initialY, 2));

  for (let i = 1; i < coords.length; i++) {
    const vectorX: number = coords[i].x - coords[i - 1].x;
    const vectorY: number = coords[i].y - coords[i - 1].y;

    const length: number = Math.sqrt(
      Math.pow(vectorX, 2) + Math.pow(vectorY, 2)
    );
    result += length;
  }

  return roundNumber(result);
};

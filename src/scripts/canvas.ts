import { CoordsType } from './types.js';

const panel: HTMLDivElement = document.querySelector(
  '.panel'
) as HTMLDivElement;
const canvas: HTMLCanvasElement = document.getElementById(
  'canvas'
) as HTMLCanvasElement;
const ctx: CanvasRenderingContext2D = canvas.getContext(
  '2d'
) as CanvasRenderingContext2D;

export const setUpCanvasSize = () => {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight - panel.offsetHeight;
};

setUpCanvasSize();

export const drawPoint = (pointX: number, pointY: number): void => {
  ctx.beginPath();
  ctx.arc(pointX, pointY, 2, 0, 2 * Math.PI, true);
  ctx.fillStyle = '#000';
  ctx.fill();
  ctx.closePath();
};

export const drawFigure = (coords: CoordsType): void => {
  ctx.fillStyle = '#4a4e69';
  ctx.beginPath();
  ctx.moveTo(coords[0].x, coords[0].y);

  for (let i = 1; i < coords.length; i++) {
    ctx.lineTo(coords[i].x, coords[i].y);
  }

  ctx.closePath();
  ctx.fill();
};

export const clearInterface = (coords: CoordsType) => {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  coords.length = 0;
};

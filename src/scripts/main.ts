import { polygonArea, calculatePerimeter } from './helpers.js';
import {
  drawPoint,
  drawFigure,
  clearInterface,
  setUpCanvasSize,
} from './canvas.js';
import { CoordsType } from './types.js';

const overlay: HTMLDivElement = document.querySelector(
  '.overlay'
) as HTMLDivElement;
const modal: HTMLDivElement = document.querySelector(
  '.modal'
) as HTMLDivElement;
const modalContent: HTMLDivElement = document.querySelector(
  '.modal-content'
) as HTMLDivElement;

export const showModal = (content: string, error = false) => {
  modalContent.innerHTML = content;
  overlay.classList.remove('hidden');
  modal.classList.remove('hidden');

  if (error) {
    modal.classList.add('error');
  }
};

export const handleCloseModal = () => {
  overlay.classList.add('hidden');
  modal.classList.add('hidden');

  if (!modal.classList.contains('error')) {
    clearInterface(coords);
  }

  modal.classList.remove('error');
};

const coords: CoordsType = [];

export const handleClick = (event: MouseEvent) => {
  if (coords.length <= 100) {
    const pointX: number = event.clientX,
      pointY: number = event.clientY;

    drawPoint(pointX, pointY);
    coords.push({ x: pointX, y: pointY });
  } else {
    showModal('Maximum 100 points are allowed!', true);
  }
};

export const handleConfirm = () => {
  if (coords.length > 2) {
    drawFigure(coords);

    const perimeter: number = calculatePerimeter(coords);
    const area: number = polygonArea(coords);
    coords.length = 0;
    showModal(`Perimeter: ${perimeter} <br /> Area: ${area}`);
  } else {
    showModal('Minimum 3 points are needed!', true);
  }
};

export const handleClear = () => {
  clearInterface(coords);
};

export const handleResize = () => {
  setUpCanvasSize();
};

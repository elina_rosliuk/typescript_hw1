import {
  handleClick,
  handleConfirm,
  handleClear,
  handleCloseModal,
  handleResize,
} from './main.js';

const canvas: HTMLCanvasElement = document.getElementById(
  'canvas'
) as HTMLCanvasElement;
const confirmButton: HTMLButtonElement = document.querySelector(
  '.confirm-button'
) as HTMLButtonElement;
const closeModalButton: HTMLButtonElement = document.querySelector(
  '.close-modal-button'
) as HTMLButtonElement;
const clearButton: HTMLButtonElement = document.querySelector(
  '.clear-button'
) as HTMLButtonElement;

canvas.addEventListener('click', handleClick);
confirmButton.addEventListener('click', handleConfirm);
closeModalButton.addEventListener('click', handleCloseModal);
clearButton.addEventListener('click', handleClear);
window.addEventListener('resize', handleResize);

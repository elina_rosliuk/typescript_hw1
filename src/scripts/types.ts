export interface IPoint {
  x: number;
  y: number;
}

export type CoordsType = Array<IPoint>;
